public class ForLoopStarDesign{
	public static void main (String args[]){
		int num=5;
		double size=Math.pow(num,2);
			for(int i=1; i<=size; i++){
		System.out.print(" * ");
			if (i%num==0){
		System.out.println(" ");
		
		
}
}		
}
/* 
output
 *  *  *  *  *
 *  *  *  *  *
 *  *  *  *  *
 *  *  *  *  *
 *  *  *  *  *
 
*/
}