package family.bank;

public class BankAccount {

	public static final String bankName = "Bank Of Ceylon  BOC ";
	private String branch; 
	private String accountType;
	private String accountNumber;
	private String accountHolderName;
	private double accountBalance=0;
	
	public void accountOpen(String branch,String accountType, String accountNumber, String accountHolderName,int amount ) {
		
		this.branch= branch;
		this.accountType= accountType;
		this.accountNumber= accountNumber;
		this.accountHolderName = accountHolderName;
		this.accountBalance = amount;
		
	}
	
	public void withdrawal(double amount) {
		accountBalance = accountBalance - amount;
		
	}
	
	public void deposit(double amount) {
		accountBalance = accountBalance + amount ;
		
	}
	 
	public double currentAccountBalance() {
		return accountBalance;
		
	}
	
	public String getAccountHolderName() {
		return  accountHolderName;
		
	}
	
	public void getAccountDetails() {
		
		System.out.println("BANK NAME = " + bankName + branch );
		System.out.println("ACCOUNT NUMBER = " + accountNumber );
		System.out.println("ACCOUNT TYPE = " + accountType );
		System.out.println("ACCOUNT BALANCE = " + accountBalance );
		System.out.println("********************************************");
	}
}

