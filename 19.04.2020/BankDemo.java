import family.bank.BankAccount;

public class BankDemo {
	public static void main (String[] args) {
		BankAccount accountDilukshika = new BankAccount();
		BankAccount accountInthusha = new BankAccount();
		BankAccount accountJenze = new BankAccount();
		BankAccount accountSivanathan = new BankAccount();
		
		
		accountDilukshika.accountOpen("Jaffna"," Saving", "150007896", "S.Dilukshika", 6200);
		accountInthusha.accountOpen("JAffna", "Saving", "5800789652", "S.Inthusha", 10000);
		accountJenze.accountOpen("JAffna", "Saving", "5844789652", "S.Jenze", 100000);
		accountSivanathan.accountOpen("JAffna", "Saving", "5475789652", "P.Sivanathan", 450500);
		
		accountDilukshika.getAccountDetails();
		accountInthusha.getAccountDetails();
		accountJenze.getAccountDetails();
		accountSivanathan.getAccountDetails();
		
		accountDilukshika.withdrawal(1000);
		accountInthusha.deposit(45000);
		accountJenze.deposit(12000);
		accountSivanathan.withdrawal(15000);
	}
/*
 BANK NAME = Bank Of Ceylon  BOC Jaffna
ACCOUNT NUMBER = 150007896
ACCOUNT TYPE =  Saving
ACCOUNT BALANCE = 6200.0
********************************************
BANK NAME = Bank Of Ceylon  BOC JAffna
ACCOUNT NUMBER = 5800789652
ACCOUNT TYPE = Saving
ACCOUNT BALANCE = 10000.0
********************************************
BANK NAME = Bank Of Ceylon  BOC JAffna
ACCOUNT NUMBER = 5844789652
ACCOUNT TYPE = Saving
ACCOUNT BALANCE = 100000.0
********************************************
BANK NAME = Bank Of Ceylon  BOC JAffna
ACCOUNT NUMBER = 5475789652
ACCOUNT TYPE = Saving
ACCOUNT BALANCE = 450500.0
********************************************s
 */

}
