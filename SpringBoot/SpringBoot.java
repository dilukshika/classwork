public class SpringBoot {
	public static void main(String args[]){
    System.out.println(" ");
		System.out.println("  '     _____          _            __ _ _ ");
   		System.out.println("  /\\\\  / ____'_ __ _ _(_)_ __  __ _ \\ \\ \\ \\");
    		System.out.println(" ( ( ) \\___  | '_ | '_| | '_ \\/ _ '| \\ \\ \\ \\");
   		System.out.println("  \\\\/   ___) | | )| | | | | || ( | |  ) ) ) )");
   		System.out.println("       |_____| ,__|_| |_|_| |_\\__ ,| / / / /");
   		System.out.println("  |==========|_|==============|___/=/_/_/_/");
   		System.out.println("  ::Spring boot ::           (v2.0.0.RELESE)");

}
/*
output
  '     _____          _            __ _ _
  /\\  / ____'_ __ _ _(_)_ __  __ _ \ \ \ \
 ( ( ) \___  | '_ | '_| | '_ \/ _ '| \ \ \ \
  \\/   ___) | | )| | | | | || ( | |  ) ) ) )
       |_____| ,__|_| |_|_| |_\__ ,| / / / /
  |==========|_|==============|___/=/_/_/_/
  ::Spring boot ::           (v2.0.0.RELESE)
*/
}